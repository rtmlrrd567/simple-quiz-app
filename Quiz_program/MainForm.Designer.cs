﻿/*
 * Created by SharpDevelop.
 * User: Evanice
 * Date: 9/20/2022
 * Time: 6:54 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace Quiz_program
{
	partial class MainForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.radioButton2 = new System.Windows.Forms.RadioButton();
			this.radioButton1 = new System.Windows.Forms.RadioButton();
			this.label1 = new System.Windows.Forms.Label();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.radioButton3 = new System.Windows.Forms.RadioButton();
			this.radioButton4 = new System.Windows.Forms.RadioButton();
			this.label2 = new System.Windows.Forms.Label();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.radioButton6 = new System.Windows.Forms.RadioButton();
			this.radioButton5 = new System.Windows.Forms.RadioButton();
			this.label3 = new System.Windows.Forms.Label();
			this.groupBox4 = new System.Windows.Forms.GroupBox();
			this.radioButton8 = new System.Windows.Forms.RadioButton();
			this.radioButton7 = new System.Windows.Forms.RadioButton();
			this.label4 = new System.Windows.Forms.Label();
			this.groupBox5 = new System.Windows.Forms.GroupBox();
			this.radioButton10 = new System.Windows.Forms.RadioButton();
			this.radioButton9 = new System.Windows.Forms.RadioButton();
			this.label5 = new System.Windows.Forms.Label();
			this.groupBox6 = new System.Windows.Forms.GroupBox();
			this.radioButton12 = new System.Windows.Forms.RadioButton();
			this.radioButton11 = new System.Windows.Forms.RadioButton();
			this.label6 = new System.Windows.Forms.Label();
			this.groupBox7 = new System.Windows.Forms.GroupBox();
			this.label7 = new System.Windows.Forms.Label();
			this.groupBox8 = new System.Windows.Forms.GroupBox();
			this.radioButton16 = new System.Windows.Forms.RadioButton();
			this.radioButton15 = new System.Windows.Forms.RadioButton();
			this.label8 = new System.Windows.Forms.Label();
			this.groupBox9 = new System.Windows.Forms.GroupBox();
			this.label9 = new System.Windows.Forms.Label();
			this.groupBox10 = new System.Windows.Forms.GroupBox();
			this.radioButton20 = new System.Windows.Forms.RadioButton();
			this.radioButton19 = new System.Windows.Forms.RadioButton();
			this.label10 = new System.Windows.Forms.Label();
			this.groupBox11 = new System.Windows.Forms.GroupBox();
			this.radioButton14 = new System.Windows.Forms.RadioButton();
			this.radioButton13 = new System.Windows.Forms.RadioButton();
			this.label11 = new System.Windows.Forms.Label();
			this.groupBox12 = new System.Windows.Forms.GroupBox();
			this.label12 = new System.Windows.Forms.Label();
			this.groupBox13 = new System.Windows.Forms.GroupBox();
			this.radioButton18 = new System.Windows.Forms.RadioButton();
			this.radioButton17 = new System.Windows.Forms.RadioButton();
			this.label13 = new System.Windows.Forms.Label();
			this.groupBox14 = new System.Windows.Forms.GroupBox();
			this.label14 = new System.Windows.Forms.Label();
			this.label15 = new System.Windows.Forms.Label();
			this.label16 = new System.Windows.Forms.Label();
			this.label17 = new System.Windows.Forms.Label();
			this.button1 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.ScoreOutput = new System.Windows.Forms.Label();
			this.Grade = new System.Windows.Forms.Label();
			this.Remarks = new System.Windows.Forms.Label();
			this.groupBox1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.groupBox3.SuspendLayout();
			this.groupBox4.SuspendLayout();
			this.groupBox5.SuspendLayout();
			this.groupBox6.SuspendLayout();
			this.groupBox7.SuspendLayout();
			this.groupBox8.SuspendLayout();
			this.groupBox9.SuspendLayout();
			this.groupBox10.SuspendLayout();
			this.groupBox11.SuspendLayout();
			this.groupBox12.SuspendLayout();
			this.groupBox13.SuspendLayout();
			this.groupBox14.SuspendLayout();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.radioButton2);
			this.groupBox1.Controls.Add(this.radioButton1);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Location = new System.Drawing.Point(24, 12);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(430, 68);
			this.groupBox1.TabIndex = 1;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Question no.1";
			// 
			// radioButton2
			// 
			this.radioButton2.Location = new System.Drawing.Point(238, 38);
			this.radioButton2.Name = "radioButton2";
			this.radioButton2.Size = new System.Drawing.Size(104, 24);
			this.radioButton2.TabIndex = 2;
			this.radioButton2.Text = "False";
			// 
			// radioButton1
			// 
			this.radioButton1.Location = new System.Drawing.Point(137, 38);
			this.radioButton1.Name = "radioButton1";
			this.radioButton1.Size = new System.Drawing.Size(95, 24);
			this.radioButton1.TabIndex = 1;
			this.radioButton1.Text = "True";
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(104, 16);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(238, 23);
			this.label1.TabIndex = 0;
			this.label1.Text = "Electrons are larger than molecules.";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.radioButton3);
			this.groupBox2.Controls.Add(this.radioButton4);
			this.groupBox2.Controls.Add(this.label2);
			this.groupBox2.Location = new System.Drawing.Point(24, 80);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(430, 68);
			this.groupBox2.TabIndex = 2;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Question no.2";
			// 
			// radioButton3
			// 
			this.radioButton3.Location = new System.Drawing.Point(137, 38);
			this.radioButton3.Name = "radioButton3";
			this.radioButton3.Size = new System.Drawing.Size(95, 24);
			this.radioButton3.TabIndex = 3;
			this.radioButton3.Text = "True";
			this.radioButton3.UseVisualStyleBackColor = true;
			// 
			// radioButton4
			// 
			this.radioButton4.Location = new System.Drawing.Point(238, 38);
			this.radioButton4.Name = "radioButton4";
			this.radioButton4.Size = new System.Drawing.Size(104, 24);
			this.radioButton4.TabIndex = 4;
			this.radioButton4.Text = "False";
			this.radioButton4.UseVisualStyleBackColor = true;
			// 
			// label2
			// 
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.Location = new System.Drawing.Point(47, 16);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(330, 23);
			this.label2.TabIndex = 0;
			this.label2.Text = "The Atlantic Ocean is the biggest ocean in the Earth.";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// groupBox3
			// 
			this.groupBox3.Controls.Add(this.radioButton6);
			this.groupBox3.Controls.Add(this.radioButton5);
			this.groupBox3.Controls.Add(this.label3);
			this.groupBox3.Location = new System.Drawing.Point(24, 148);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(430, 68);
			this.groupBox3.TabIndex = 3;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "Question no.3";
			// 
			// radioButton6
			// 
			this.radioButton6.Location = new System.Drawing.Point(238, 38);
			this.radioButton6.Name = "radioButton6";
			this.radioButton6.Size = new System.Drawing.Size(104, 24);
			this.radioButton6.TabIndex = 6;
			this.radioButton6.Text = "False";
			this.radioButton6.UseVisualStyleBackColor = true;
			// 
			// radioButton5
			// 
			this.radioButton5.Location = new System.Drawing.Point(137, 38);
			this.radioButton5.Name = "radioButton5";
			this.radioButton5.Size = new System.Drawing.Size(95, 24);
			this.radioButton5.TabIndex = 5;
			this.radioButton5.Text = "True";
			this.radioButton5.UseVisualStyleBackColor = true;
			// 
			// label3
			// 
			this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label3.Location = new System.Drawing.Point(26, 16);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(383, 23);
			this.label3.TabIndex = 0;
			this.label3.Text = "The chemical make up food often changes when you cook it.";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// groupBox4
			// 
			this.groupBox4.Controls.Add(this.radioButton8);
			this.groupBox4.Controls.Add(this.radioButton7);
			this.groupBox4.Controls.Add(this.label4);
			this.groupBox4.Location = new System.Drawing.Point(24, 216);
			this.groupBox4.Name = "groupBox4";
			this.groupBox4.Size = new System.Drawing.Size(430, 68);
			this.groupBox4.TabIndex = 4;
			this.groupBox4.TabStop = false;
			this.groupBox4.Text = "Question no.4";
			// 
			// radioButton8
			// 
			this.radioButton8.Location = new System.Drawing.Point(238, 38);
			this.radioButton8.Name = "radioButton8";
			this.radioButton8.Size = new System.Drawing.Size(104, 24);
			this.radioButton8.TabIndex = 8;
			this.radioButton8.Text = "False";
			this.radioButton8.UseVisualStyleBackColor = true;
			// 
			// radioButton7
			// 
			this.radioButton7.Location = new System.Drawing.Point(137, 38);
			this.radioButton7.Name = "radioButton7";
			this.radioButton7.Size = new System.Drawing.Size(95, 24);
			this.radioButton7.TabIndex = 7;
			this.radioButton7.Text = "True";
			this.radioButton7.UseVisualStyleBackColor = true;
			// 
			// label4
			// 
			this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label4.Location = new System.Drawing.Point(26, 16);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(383, 23);
			this.label4.TabIndex = 0;
			this.label4.Text = "Sharks are mammals.";
			this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// groupBox5
			// 
			this.groupBox5.Controls.Add(this.radioButton10);
			this.groupBox5.Controls.Add(this.radioButton9);
			this.groupBox5.Controls.Add(this.label5);
			this.groupBox5.Location = new System.Drawing.Point(24, 284);
			this.groupBox5.Name = "groupBox5";
			this.groupBox5.Size = new System.Drawing.Size(430, 68);
			this.groupBox5.TabIndex = 5;
			this.groupBox5.TabStop = false;
			this.groupBox5.Text = "Question no.5";
			// 
			// radioButton10
			// 
			this.radioButton10.Location = new System.Drawing.Point(238, 38);
			this.radioButton10.Name = "radioButton10";
			this.radioButton10.Size = new System.Drawing.Size(104, 24);
			this.radioButton10.TabIndex = 10;
			this.radioButton10.Text = "False";
			this.radioButton10.UseVisualStyleBackColor = true;
			// 
			// radioButton9
			// 
			this.radioButton9.Location = new System.Drawing.Point(137, 38);
			this.radioButton9.Name = "radioButton9";
			this.radioButton9.Size = new System.Drawing.Size(95, 24);
			this.radioButton9.TabIndex = 9;
			this.radioButton9.Text = "True";
			this.radioButton9.UseVisualStyleBackColor = true;
			// 
			// label5
			// 
			this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label5.Location = new System.Drawing.Point(26, 16);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(383, 23);
			this.label5.TabIndex = 0;
			this.label5.Text = "The human body has four lungs.";
			this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// groupBox6
			// 
			this.groupBox6.Controls.Add(this.radioButton12);
			this.groupBox6.Controls.Add(this.radioButton11);
			this.groupBox6.Controls.Add(this.label6);
			this.groupBox6.Controls.Add(this.groupBox7);
			this.groupBox6.Location = new System.Drawing.Point(24, 352);
			this.groupBox6.Name = "groupBox6";
			this.groupBox6.Size = new System.Drawing.Size(430, 68);
			this.groupBox6.TabIndex = 6;
			this.groupBox6.TabStop = false;
			this.groupBox6.Text = "Question no.6";
			// 
			// radioButton12
			// 
			this.radioButton12.Location = new System.Drawing.Point(238, 38);
			this.radioButton12.Name = "radioButton12";
			this.radioButton12.Size = new System.Drawing.Size(104, 24);
			this.radioButton12.TabIndex = 12;
			this.radioButton12.Text = "False";
			this.radioButton12.UseVisualStyleBackColor = true;
			// 
			// radioButton11
			// 
			this.radioButton11.Location = new System.Drawing.Point(137, 38);
			this.radioButton11.Name = "radioButton11";
			this.radioButton11.Size = new System.Drawing.Size(95, 24);
			this.radioButton11.TabIndex = 11;
			this.radioButton11.Text = "True";
			this.radioButton11.UseVisualStyleBackColor = true;
			// 
			// label6
			// 
			this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label6.Location = new System.Drawing.Point(26, 16);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(383, 23);
			this.label6.TabIndex = 0;
			this.label6.Text = "Atoms are more stable when their outer shells are full.";
			this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// groupBox7
			// 
			this.groupBox7.Controls.Add(this.label7);
			this.groupBox7.Location = new System.Drawing.Point(0, 68);
			this.groupBox7.Name = "groupBox7";
			this.groupBox7.Size = new System.Drawing.Size(430, 68);
			this.groupBox7.TabIndex = 7;
			this.groupBox7.TabStop = false;
			this.groupBox7.Text = "Question no.7";
			// 
			// label7
			// 
			this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label7.Location = new System.Drawing.Point(26, 16);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(383, 23);
			this.label7.TabIndex = 0;
			this.label7.Text = "Filtration separates mixtures based upon their particle size.";
			this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// groupBox8
			// 
			this.groupBox8.Controls.Add(this.radioButton16);
			this.groupBox8.Controls.Add(this.radioButton15);
			this.groupBox8.Controls.Add(this.label8);
			this.groupBox8.Controls.Add(this.groupBox9);
			this.groupBox8.Location = new System.Drawing.Point(24, 488);
			this.groupBox8.Name = "groupBox8";
			this.groupBox8.Size = new System.Drawing.Size(430, 68);
			this.groupBox8.TabIndex = 8;
			this.groupBox8.TabStop = false;
			this.groupBox8.Text = "Question no.8";
			// 
			// radioButton16
			// 
			this.radioButton16.Location = new System.Drawing.Point(238, 38);
			this.radioButton16.Name = "radioButton16";
			this.radioButton16.Size = new System.Drawing.Size(104, 24);
			this.radioButton16.TabIndex = 16;
			this.radioButton16.Text = "False";
			this.radioButton16.UseVisualStyleBackColor = true;
			// 
			// radioButton15
			// 
			this.radioButton15.Location = new System.Drawing.Point(137, 38);
			this.radioButton15.Name = "radioButton15";
			this.radioButton15.Size = new System.Drawing.Size(95, 24);
			this.radioButton15.TabIndex = 15;
			this.radioButton15.Text = "True";
			this.radioButton15.UseVisualStyleBackColor = true;
			// 
			// label8
			// 
			this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label8.Location = new System.Drawing.Point(26, 16);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(383, 23);
			this.label8.TabIndex = 0;
			this.label8.Text = "Venus is the closest planet to the Sun.";
			this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// groupBox9
			// 
			this.groupBox9.Controls.Add(this.label9);
			this.groupBox9.Location = new System.Drawing.Point(0, 74);
			this.groupBox9.Name = "groupBox9";
			this.groupBox9.Size = new System.Drawing.Size(430, 68);
			this.groupBox9.TabIndex = 9;
			this.groupBox9.TabStop = false;
			this.groupBox9.Text = "Question no.9";
			// 
			// label9
			// 
			this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label9.Location = new System.Drawing.Point(26, 16);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(383, 23);
			this.label9.TabIndex = 0;
			this.label9.Text = "Conductors have low resistance.";
			this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// groupBox10
			// 
			this.groupBox10.Controls.Add(this.radioButton20);
			this.groupBox10.Controls.Add(this.radioButton19);
			this.groupBox10.Controls.Add(this.label10);
			this.groupBox10.Location = new System.Drawing.Point(24, 624);
			this.groupBox10.Name = "groupBox10";
			this.groupBox10.Size = new System.Drawing.Size(430, 68);
			this.groupBox10.TabIndex = 10;
			this.groupBox10.TabStop = false;
			this.groupBox10.Text = "Question no.10";
			// 
			// radioButton20
			// 
			this.radioButton20.Location = new System.Drawing.Point(238, 38);
			this.radioButton20.Name = "radioButton20";
			this.radioButton20.Size = new System.Drawing.Size(104, 24);
			this.radioButton20.TabIndex = 20;
			this.radioButton20.Text = "False";
			this.radioButton20.UseVisualStyleBackColor = true;
			// 
			// radioButton19
			// 
			this.radioButton19.Location = new System.Drawing.Point(137, 38);
			this.radioButton19.Name = "radioButton19";
			this.radioButton19.Size = new System.Drawing.Size(95, 24);
			this.radioButton19.TabIndex = 19;
			this.radioButton19.Text = "True";
			this.radioButton19.UseVisualStyleBackColor = true;
			// 
			// label10
			// 
			this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label10.Location = new System.Drawing.Point(6, 16);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(418, 23);
			this.label10.TabIndex = 0;
			this.label10.Text = "Molecules can have atoms from more than one chemical element.";
			this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// groupBox11
			// 
			this.groupBox11.Controls.Add(this.radioButton14);
			this.groupBox11.Controls.Add(this.radioButton13);
			this.groupBox11.Controls.Add(this.label11);
			this.groupBox11.Controls.Add(this.groupBox12);
			this.groupBox11.Location = new System.Drawing.Point(24, 420);
			this.groupBox11.Name = "groupBox11";
			this.groupBox11.Size = new System.Drawing.Size(430, 68);
			this.groupBox11.TabIndex = 8;
			this.groupBox11.TabStop = false;
			this.groupBox11.Text = "Question no.7";
			// 
			// radioButton14
			// 
			this.radioButton14.Location = new System.Drawing.Point(238, 38);
			this.radioButton14.Name = "radioButton14";
			this.radioButton14.Size = new System.Drawing.Size(104, 24);
			this.radioButton14.TabIndex = 14;
			this.radioButton14.Text = "False";
			this.radioButton14.UseVisualStyleBackColor = true;
			// 
			// radioButton13
			// 
			this.radioButton13.Location = new System.Drawing.Point(137, 38);
			this.radioButton13.Name = "radioButton13";
			this.radioButton13.Size = new System.Drawing.Size(95, 24);
			this.radioButton13.TabIndex = 13;
			this.radioButton13.Text = "True";
			this.radioButton13.UseVisualStyleBackColor = true;
			// 
			// label11
			// 
			this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label11.Location = new System.Drawing.Point(26, 16);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(383, 23);
			this.label11.TabIndex = 0;
			this.label11.Text = "Filtration separates mixtures based upon their particle size.";
			this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// groupBox12
			// 
			this.groupBox12.Controls.Add(this.label12);
			this.groupBox12.Location = new System.Drawing.Point(0, 68);
			this.groupBox12.Name = "groupBox12";
			this.groupBox12.Size = new System.Drawing.Size(430, 68);
			this.groupBox12.TabIndex = 7;
			this.groupBox12.TabStop = false;
			this.groupBox12.Text = "Question no.7";
			// 
			// label12
			// 
			this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label12.Location = new System.Drawing.Point(26, 16);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(383, 23);
			this.label12.TabIndex = 0;
			this.label12.Text = "Filtration separates mixtures based upon their particle size.";
			this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// groupBox13
			// 
			this.groupBox13.Controls.Add(this.radioButton18);
			this.groupBox13.Controls.Add(this.radioButton17);
			this.groupBox13.Controls.Add(this.label13);
			this.groupBox13.Controls.Add(this.groupBox14);
			this.groupBox13.Location = new System.Drawing.Point(24, 556);
			this.groupBox13.Name = "groupBox13";
			this.groupBox13.Size = new System.Drawing.Size(430, 68);
			this.groupBox13.TabIndex = 10;
			this.groupBox13.TabStop = false;
			this.groupBox13.Text = "Question no.9";
			// 
			// radioButton18
			// 
			this.radioButton18.Location = new System.Drawing.Point(238, 38);
			this.radioButton18.Name = "radioButton18";
			this.radioButton18.Size = new System.Drawing.Size(104, 24);
			this.radioButton18.TabIndex = 18;
			this.radioButton18.Text = "False";
			this.radioButton18.UseVisualStyleBackColor = true;
			// 
			// radioButton17
			// 
			this.radioButton17.Location = new System.Drawing.Point(137, 38);
			this.radioButton17.Name = "radioButton17";
			this.radioButton17.Size = new System.Drawing.Size(95, 24);
			this.radioButton17.TabIndex = 17;
			this.radioButton17.Text = "True";
			this.radioButton17.UseVisualStyleBackColor = true;
			// 
			// label13
			// 
			this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label13.Location = new System.Drawing.Point(26, 16);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(383, 23);
			this.label13.TabIndex = 0;
			this.label13.Text = "Conductors have low resistance.";
			this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// groupBox14
			// 
			this.groupBox14.Controls.Add(this.label14);
			this.groupBox14.Location = new System.Drawing.Point(0, 74);
			this.groupBox14.Name = "groupBox14";
			this.groupBox14.Size = new System.Drawing.Size(430, 68);
			this.groupBox14.TabIndex = 9;
			this.groupBox14.TabStop = false;
			this.groupBox14.Text = "Question no.9";
			// 
			// label14
			// 
			this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label14.Location = new System.Drawing.Point(26, 16);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(383, 23);
			this.label14.TabIndex = 0;
			this.label14.Text = "Conductors have low resistance.";
			this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label15
			// 
			this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label15.Location = new System.Drawing.Point(519, 28);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(100, 23);
			this.label15.TabIndex = 11;
			this.label15.Text = "Score:";
			// 
			// label16
			// 
			this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label16.Location = new System.Drawing.Point(519, 118);
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size(100, 23);
			this.label16.TabIndex = 12;
			this.label16.Text = "Remarks:";
			// 
			// label17
			// 
			this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label17.Location = new System.Drawing.Point(519, 71);
			this.label17.Name = "label17";
			this.label17.Size = new System.Drawing.Size(100, 23);
			this.label17.TabIndex = 13;
			this.label17.Text = "Grade:";
			// 
			// button1
			// 
			this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.button1.Location = new System.Drawing.Point(519, 186);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(105, 48);
			this.button1.TabIndex = 14;
			this.button1.Text = "Submit";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.Button1Click);
			// 
			// button2
			// 
			this.button2.Enabled = false;
			this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.button2.Location = new System.Drawing.Point(654, 186);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(105, 48);
			this.button2.TabIndex = 15;
			this.button2.Text = "Retry";
			this.button2.UseVisualStyleBackColor = true;
			this.button2.Click += new System.EventHandler(this.Button2Click);
			// 
			// ScoreOutput
			// 
			this.ScoreOutput.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.ScoreOutput.Location = new System.Drawing.Point(639, 28);
			this.ScoreOutput.Name = "ScoreOutput";
			this.ScoreOutput.Size = new System.Drawing.Size(100, 23);
			this.ScoreOutput.TabIndex = 16;
			// 
			// Grade
			// 
			this.Grade.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Grade.Location = new System.Drawing.Point(639, 71);
			this.Grade.Name = "Grade";
			this.Grade.Size = new System.Drawing.Size(100, 23);
			this.Grade.TabIndex = 17;
			// 
			// Remarks
			// 
			this.Remarks.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Remarks.Location = new System.Drawing.Point(639, 118);
			this.Remarks.Name = "Remarks";
			this.Remarks.Size = new System.Drawing.Size(100, 23);
			this.Remarks.TabIndex = 18;
			// 
			// MainForm
			// 
			this.ClientSize = new System.Drawing.Size(771, 721);
			this.Controls.Add(this.Remarks);
			this.Controls.Add(this.Grade);
			this.Controls.Add(this.ScoreOutput);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.label17);
			this.Controls.Add(this.label16);
			this.Controls.Add(this.label15);
			this.Controls.Add(this.groupBox13);
			this.Controls.Add(this.groupBox11);
			this.Controls.Add(this.groupBox10);
			this.Controls.Add(this.groupBox8);
			this.Controls.Add(this.groupBox5);
			this.Controls.Add(this.groupBox6);
			this.Controls.Add(this.groupBox4);
			this.Controls.Add(this.groupBox3);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.groupBox1);
			this.Name = "MainForm";
			this.Text = "Quiz";
			this.groupBox1.ResumeLayout(false);
			this.groupBox2.ResumeLayout(false);
			this.groupBox3.ResumeLayout(false);
			this.groupBox4.ResumeLayout(false);
			this.groupBox5.ResumeLayout(false);
			this.groupBox6.ResumeLayout(false);
			this.groupBox7.ResumeLayout(false);
			this.groupBox8.ResumeLayout(false);
			this.groupBox9.ResumeLayout(false);
			this.groupBox10.ResumeLayout(false);
			this.groupBox11.ResumeLayout(false);
			this.groupBox12.ResumeLayout(false);
			this.groupBox13.ResumeLayout(false);
			this.groupBox14.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.Label Remarks;
		private System.Windows.Forms.Label Grade;
		private System.Windows.Forms.Label ScoreOutput;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.GroupBox groupBox14;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.RadioButton radioButton17;
		private System.Windows.Forms.RadioButton radioButton18;
		private System.Windows.Forms.GroupBox groupBox13;
		private System.Windows.Forms.RadioButton radioButton13;
		private System.Windows.Forms.RadioButton radioButton14;
		private System.Windows.Forms.RadioButton radioButton19;
		private System.Windows.Forms.RadioButton radioButton20;
		private System.Windows.Forms.RadioButton radioButton15;
		private System.Windows.Forms.RadioButton radioButton16;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.GroupBox groupBox12;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.GroupBox groupBox11;
		private System.Windows.Forms.RadioButton radioButton11;
		private System.Windows.Forms.RadioButton radioButton12;
		private System.Windows.Forms.RadioButton radioButton9;
		private System.Windows.Forms.RadioButton radioButton10;
		private System.Windows.Forms.RadioButton radioButton7;
		private System.Windows.Forms.RadioButton radioButton8;
		private System.Windows.Forms.RadioButton radioButton6;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.GroupBox groupBox10;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.GroupBox groupBox9;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.GroupBox groupBox8;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.GroupBox groupBox7;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.GroupBox groupBox6;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.GroupBox groupBox5;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.GroupBox groupBox4;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.RadioButton radioButton5;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.RadioButton radioButton4;
		private System.Windows.Forms.RadioButton radioButton3;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.RadioButton radioButton1;
		private System.Windows.Forms.RadioButton radioButton2;
		private System.Windows.Forms.GroupBox groupBox1;
	}
}
