﻿/*
 * Created by SharpDevelop.
 * User: Evanice
 * Date: 9/20/2022
 * Time: 6:54 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Quiz_program
{
	/// <summary>
	/// Description of MainForm.
	/// </summary>
	public partial class MainForm : Form
	{
		double score = 0;
		double percentage;
		public MainForm()
		{
			InitializeComponent();
		}
		
		void Button1Click(object sender, EventArgs e)
		{
			if(radioButton2.Checked == true){
				score++;
			}
			if(radioButton4.Checked == true){
				score++;
			}
			if(radioButton5.Checked == true){
				score++;
			}
			if(radioButton8.Checked == true){
				score++;
			}
			if(radioButton10.Checked == true){
				score++;
			}
			if(radioButton11.Checked == true){
				score++;
			}
			if(radioButton13.Checked == true){
				score++;
			}
			if(radioButton16.Checked == true){
				score++;
			}
			if(radioButton17.Checked == true){
				score++;
			}
			if(radioButton19.Checked == true){
				score++;
			}
			percentage =  score / 10 * 50 + 50;
			ScoreOutput.Text = score.ToString() + "/10";
			Grade.Text = percentage.ToString() + "%";
			if(percentage >= 75){
				Remarks.Text = "Passed";
				ScoreOutput.ForeColor = Color.Green;
				Grade.ForeColor = Color.Green;
				Remarks.ForeColor = Color.Green;
			}
			else{
				Remarks.Text = "Failed";
				ScoreOutput.ForeColor = Color.Red;
				Grade.ForeColor = Color.Red;
				Remarks.ForeColor = Color.Red;
			}
			button1.Enabled = false;
			groupBox1.Enabled = false;
			groupBox2.Enabled = false;
			groupBox3.Enabled = false;
			groupBox4.Enabled = false;
			groupBox5.Enabled = false;
			groupBox6.Enabled = false;
			groupBox11.Enabled = false;
			groupBox8.Enabled = false;
			groupBox13.Enabled = false;
			groupBox10.Enabled = false;
			button2.Enabled = true;
		}

			
		
		void Button2Click(object sender, EventArgs e)
		{
			button2.Enabled = false;
			button1.Enabled = true;
			groupBox1.Enabled = true;
			groupBox2.Enabled = true;
			groupBox3.Enabled = true;
			groupBox4.Enabled = true;
			groupBox5.Enabled = true;
			groupBox6.Enabled = true;
			groupBox11.Enabled = true;
			groupBox8.Enabled = true;
			groupBox13.Enabled = true;
			groupBox10.Enabled = true;
			radioButton1.Checked = false;
			radioButton2.Checked = false;
			radioButton3.Checked = false;
			radioButton4.Checked = false;
			radioButton5.Checked = false;
			radioButton6.Checked = false;
			radioButton7.Checked = false;
			radioButton8.Checked = false;
			radioButton9.Checked = false;
			radioButton10.Checked = false;
			radioButton11.Checked = false;
			radioButton12.Checked = false;
			radioButton13.Checked = false;
			radioButton14.Checked = false;
			radioButton15.Checked = false;
			radioButton16.Checked = false;
			radioButton17.Checked = false;
			radioButton18.Checked = false;
			radioButton19.Checked = false;
			radioButton20.Checked = false;
			ScoreOutput.Text = " ";
			Grade.Text = " ";
			Remarks.Text = " ";
			score = 0;			
		}
	}
}